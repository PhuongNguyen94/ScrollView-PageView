//
//  ViewController.swift
//  ScrollView-PageView
//
//  Created by Administrator on 1/25/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIScrollViewDelegate {
    

    
    
    
    
    @IBOutlet weak var ScrollView : UIScrollView!
    var photo = UIImageView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageView = UIImageView(image: UIImage(named:"spiderMan"))
        imageView.frame = CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height)
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.isMultipleTouchEnabled = true
        let Tap = UITapGestureRecognizer(target: self, action: #selector(tapHandle(gesture:)))
        Tap.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(Tap)
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleHandle(gesture:)))
        doubleTap.numberOfTapsRequired = 2
        imageView.addGestureRecognizer(doubleTap)
        
        //MARK : - Fail to double Tap
        Tap.require(toFail: doubleTap)
        
        
        photo = imageView
        ScrollView.minimumZoomScale = 0.5
        ScrollView.maximumZoomScale = 2
        //MARK : - Set Contensize
        ScrollView.backgroundColor = UIColor.red
        ScrollView.contentSize = CGSize(width: imageView.bounds.width+20, height: imageView.bounds.height+20)
        //MARK: - Test...
        ScrollView.bounces = true
        //MARK : - Test...
        ScrollView.contentOffset = CGPoint(x: -200, y: -50)
        
        //MARK : - Test...
        
        //ScrollView.clipsToBounds = true
        
        
        self.ScrollView.addSubview(imageView)
        ScrollView.delegate = self
    }
    @objc func tapHandle(gesture:UITapGestureRecognizer){
        
        let position = gesture.location(in: photo)
        zoomRectForScale(scale: ScrollView.zoomScale*1.5, center: position)
        
    }
    @objc func doubleHandle(gesture:UITapGestureRecognizer){
        
        let position = gesture.location(in: photo)
        zoomRectForScale(scale: ScrollView.zoomScale*0.5, center: position)
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return photo
    }
    
    //MARK : - Set size scale
    func zoomRectForScale(scale:CGFloat,center:CGPoint){
        var zoomRect = CGRect()
        let scrollViewSize = ScrollView.bounds.size
        zoomRect.size.height = scrollViewSize.height/scale
        zoomRect.size.width = scrollViewSize.width/scale
        
        //MARK : - Calculate origin location photo
        zoomRect.origin.x = center.x - (zoomRect.size.width/2.0)
        zoomRect.origin.y = center.y - (zoomRect.size.height/2.0)
        print(zoomRect)
        ScrollView.zoom(to: zoomRect, animated: true)
    }
}

